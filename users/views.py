import requests

from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from rest_framework import status
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response

from users.serializers import CreateUserSerializer


class RegisterUserAPIView(generics.CreateAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs) -> Response:
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response_with_tokens = requests.post(
            url=request.build_absolute_uri(reverse("token-obtain")), data=request.data
        )
        return Response(
            response_with_tokens.json(), status=status.HTTP_201_CREATED, headers=headers
        )

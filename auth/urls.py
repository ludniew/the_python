from django.urls import path
from rest_framework_simplejwt.views import token_obtain_pair, token_refresh


urlpatterns = [
    path("tokens/", token_obtain_pair, name="token-obtain"),
    path("tokens/refresh/", token_refresh, name="token-refresh"),
]

Background
----------

Your task is to create a simple RESTful API that exposes endpoints for creating, reading, updating, and deleting a model called `Book`. Each `Book` object should have the following fields:

-   `title` (CharField)
-   `author` (CharField)
-   `published_date` (DateField)
-   `isbn_number` (CharField)
-   `language` (CharField)

Requirements
------------

Your implementation should satisfy the following requirements:

1.  The API should be implemented using Django and Django REST framework.
2.  The API should include endpoints for creating, reading, updating, and deleting `Book` objects.
3.  The API should enforce the following validations:
    -   `title` and `author` should not be blank.
    -   `published_date` should not be in the future.
    -   `isbn_number` should be unique.
4.  The API should use JWT authentication for all endpoints except for the authentication and registration endpoints.
5.  The API should return appropriate HTTP status codes and error messages for invalid requests.
6.  The API should be tested using Django's testing framework.

Authentication Requirements
---------------------------

1.  The API should provide endpoints for authentication and registration.
2.  The authentication endpoint should accept a POST request with the following data:
    -   `username` (CharField)
    -   `password` (CharField) It should return a JSON response with the JWT token if the credentials are valid.
3.  The registration endpoint should accept a POST request with the following data:
    -   `username` (CharField)
    -   `password` (CharField) It should create a new user with the provided credentials and return a JSON response with the JWT token.
4.  All endpoints except for the authentication and registration endpoints should require a JWT token in the `Authorization` header of the request. The token should be in the format `Bearer <token>`.

Instructions
------------

1.  Fork this repository.
2.  Implement the requirements described above.
3.  Write tests for your implementation.
4.  Commit your changes and push them to your forked repository.
5.  Share the link to your forked repository with us.

Notes
-----

-   You may use any Python libraries that you like.

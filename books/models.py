from django.db import models

from books.validators import validate_published_date


class Book(models.Model):
    title = models.CharField(blank=False, max_length=256)
    author = models.CharField(blank=False, max_length=256)
    published_date = models.DateField(validators=[validate_published_date])
    isbn_number = models.CharField(unique=True, max_length=256)
    language = models.CharField(max_length=256)

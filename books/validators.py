from datetime import date
from django.core.exceptions import ValidationError


def validate_published_date(value):
    today = date.today()
    if value > today:
        raise ValidationError("Book publish date cannot be in the future.")

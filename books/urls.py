from django.urls import path

from books import views


urlpatterns = [
    path("", views.BookListCreateAPIView.as_view(), name="book-list-create"),
    path(
        "<int:pk>/",
        views.BookRetrieveUpdateDestroyAPIView.as_view(),
        name="book-retrieve-update-destroy",
    ),
]

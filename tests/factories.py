import factory
from faker import Faker
from django.contrib.auth import get_user_model

from books.models import (
    Book,
)

fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = get_user_model()

    username = factory.Sequence(lambda x: fake.user_name())
    password = factory.Sequence(lambda x: fake.password())
    is_staff = False
    is_active = True


class BookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Book

    title = factory.Sequence(lambda x: fake.word())
    author = factory.Sequence(lambda x: fake.name())
    published_date = factory.Sequence(lambda x: fake.past_date())
    isbn_number = factory.Sequence(lambda x: fake.isbn10())
    language = factory.Sequence(lambda x: fake.language_name())

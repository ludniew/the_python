import pytest
from faker import Faker

from django.shortcuts import reverse

from books.models import Book


fake = Faker()


@pytest.mark.django_db
def test_GET_BookListCreateAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-list-create")

    response = client.get(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_GET_BookListCreateAPIView_should_return_list_of_existing_books(
    authenticated_client, book_factory
):
    books = [book_factory.create() for _ in range(5)]
    target_url = reverse("book-list-create")

    response = authenticated_client.get(path=target_url)
    response_data = response.data

    assert response.status_code == 200
    assert len(response_data) == len(books)
    assert len(response_data) == Book.objects.count()


@pytest.mark.django_db
def test_POST_BookListCreateAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-list-create")

    response = client.post(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_POST_BookListCreateAPIView_should_create_new_book(authenticated_client):
    target_url = reverse("book-list-create")
    payload = {
        "title": fake.word(),
        "author": fake.name(),
        "published_date": fake.past_date(),
        "isbn_number": fake.isbn10(),
        "language": fake.language_name(),
    }

    assert Book.objects.count() == 0
    response = authenticated_client.post(path=target_url, data=payload)

    assert response.status_code == 201
    assert Book.objects.count() == 1
    assert response.data["title"] == payload["title"]
    assert response.data["author"] == payload["author"]
    assert response.data["published_date"] == str(payload["published_date"])
    assert response.data["isbn_number"] == payload["isbn_number"]
    assert response.data["language"] == payload["language"]


@pytest.mark.django_db
def test_GET_BookRetrieveUpdateDestroyAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": 1})

    response = client.get(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_GET_BookRetrieveUpdateDestroyAPIView_returns_specific_book_data(
    authenticated_client, book_factory
):
    book = book_factory.create()
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": book.id})

    response = authenticated_client.get(path=target_url)

    assert response.status_code == 200
    assert response.data["id"] == book.id
    assert response.data["title"] == book.title
    assert response.data["author"] == book.author
    assert response.data["published_date"] == str(book.published_date)
    assert response.data["isbn_number"] == book.isbn_number
    assert response.data["language"] == book.language


@pytest.mark.django_db
def test_PUT_BookRetrieveUpdateDestroyAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": 1})

    response = client.put(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_PUT_BookRetrieveUpdateDestroyAPIView_should_update_proper_book_instance(
    authenticated_client, book_factory
):
    book = book_factory.create()
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": book.id})
    payload = {
        "title": "updated_title",
        "author": "updated_author",
        "published_date": "2022-01-01",
        "isbn_number": "updated_isbn",
        "language": "updated_language",
    }

    response = authenticated_client.put(path=target_url, data=payload)

    assert response.status_code == 200
    assert Book.objects.count() == 1
    assert response.data["id"] == book.id
    assert response.data["title"] == payload["title"]
    assert response.data["author"] == payload["author"]
    assert response.data["published_date"] == str(payload["published_date"])
    assert response.data["isbn_number"] == payload["isbn_number"]
    assert response.data["language"] == payload["language"]


@pytest.mark.django_db
def test_PATCH_BookRetrieveUpdateDestroyAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": 1})

    response = client.patch(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_PATCH_BookRetrieveUpdateDestroyAPIView_should_partially_update_proper_book_instance(
    authenticated_client, book_factory
):
    book = book_factory.create()
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": book.id})
    payload = {
        "title": "updated_title",
    }

    response = authenticated_client.patch(path=target_url, data=payload)
    book_db = Book.objects.get(id=book.id)

    assert response.status_code == 200
    assert Book.objects.count() == 1
    assert response.data["id"] == book.id
    assert response.data["title"] == payload["title"]
    assert response.data["author"] == book_db.author
    assert response.data["published_date"] == str(book_db.published_date)
    assert response.data["isbn_number"] == book_db.isbn_number
    assert response.data["language"] == book_db.language


@pytest.mark.django_db
def test_DELETE_BookRetrieveUpdateDestroyAPIView_returns_status_code_401_when_not_authenticated(
    client,
):
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": 1})

    response = client.delete(path=target_url)

    assert response.status_code == 401


@pytest.mark.django_db
def test_DELETE_BookRetrieveUpdateDestroyAPIView_should_partially_update_proper_book_instance(
    authenticated_client, book_factory
):
    book = book_factory.create()
    target_url = reverse("book-retrieve-update-destroy", kwargs={"pk": book.id})

    assert Book.objects.count() == 1
    response = authenticated_client.delete(
        path=target_url,
    )

    assert response.status_code == 204
    assert Book.objects.count() == 0

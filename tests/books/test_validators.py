import pytest
from faker import Faker

from django.core.exceptions import ValidationError

from books.validators import validate_published_date

fake = Faker()


@pytest.mark.parametrize("date", [fake.past_date() for _ in range(10)])
def test_validate_published_date_when_provided_past_date_then_exception_is_not_raised(
    date,
):
    validate_published_date(value=date)


@pytest.mark.parametrize("date", [fake.future_date() for _ in range(10)])
def test_validate_published_date_when_provided_future_date_then_exception_is_raised(
    date,
):
    with pytest.raises(
        ValidationError, match="Book publish date cannot be in the future."
    ):
        validate_published_date(value=date)

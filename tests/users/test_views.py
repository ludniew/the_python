import pytest
import requests_mock
from faker import Faker

from django.shortcuts import reverse
from django.contrib.auth import get_user_model

fake = Faker()
user_model = get_user_model()


@pytest.mark.django_db
def test_POST_RegisterUserAPIView_should_create_user_and_authenticate_returning_tokens(
    client,
):
    target_url = reverse("user-create")
    tokens_url = reverse("token-obtain")
    payload = {"username": fake.user_name(), "password": fake.password()}
    response_with_tokens = {
        "access": "example_access_token",
        "refresh": "example_refresh_token",
    }

    assert user_model.objects.count() == 0
    with requests_mock.Mocker() as mocker:
        mocker.register_uri(
            url=tokens_url,
            method="POST",
            json=response_with_tokens,
            status_code=200,
        )
        response = client.post(path=target_url, data=payload)

    assert response.status_code == 201
    assert user_model.objects.count() == 1
    assert "access" in response.data
    assert "refresh" in response.data

import pytest
from pytest_factoryboy import register
from rest_framework.test import APIClient

from tests.factories import UserFactory, BookFactory


register(UserFactory)
register(BookFactory)


@pytest.fixture
def client():
    client = APIClient()
    return client


@pytest.fixture
def authenticated_client(user_factory):
    user = user_factory.create()
    client = APIClient()
    client.force_authenticate(user=user)
    return client
